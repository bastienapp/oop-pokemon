import Pokemon from './Pokemon.js';
// const Pokemon = require('./Pokemon.js')

const pikachu = {
  name: "Pikachu",

  cry: function () {
    return "Pika pika";
  },

  takeDamage: function (damage) {
    return this.name + " takes " + damage + " damages";
  },
};

// OOP : prototype
// OOP : function
// OOP : classes

// new Pokemon : appelle le constructeur de la classe
const pika = new Pokemon("Pikachu");
const cara = new Pokemon("Carapuce");
const missigno = new Pokemon();
// objet : instance d'une classe

// référence      instance
const salameche = new Pokemon("Salamèche");
const sala2 = salameche; // je n'ai pas fait un clone !
salameche.name = "Salamèche Edited";
console.log(sala2.name);

console.log("pika", pika.takeDamages(10));
console.log("cara", cara.takeDamages(2));
console.log("missigno", missigno.takeDamages(777));

// pika.#life -= 50;
// console.log("pika PV", pika.#life);