// encapsulation : rendre public ou privée des choses de la classe

class Pokemon {
  // mon pokémon aura 100 PV au moment où il est créé
  #life // la propriété est privée

  constructor(nameValue) {
    this.name = nameValue;
    this.#life = 100;
  }

  takeDamages(damages) {
    this.#life -= damages; // this.life = this.life - damages;
    if (this.#life < 0) {
      this.#life = 0;
    }
    return this.name + " takes " + damages + " damages\n"
      + this.name + " has " + this.#life + " life point";
  }
}

export default Pokemon;
// module.exports = Pokemon;